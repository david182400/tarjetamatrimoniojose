import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tarjeta_matrimonio/model/guest.dart';


class FirestoreApi {

  final String collectionName;

  final StreamController<List<Guest>> _streamController = StreamController<List<Guest>>();
  Stream<List<Guest>> get listGuestsStream => _streamController.stream;

  StreamSubscription<List<Guest>> _streamSubscription;

  FirestoreApi(this.collectionName);

  Future<bool> addEditData(Map<String, dynamic> data, bool isAdd, {String dataId}) async {
    CollectionReference users = FirebaseFirestore.instance.collection(collectionName);
    try {
      if (isAdd) {
        return users.add(data).then((value) {
          return true;
        }).catchError((error) {
          throw ('FireStore error: $error');
        });
      } else {
        return users.doc(dataId).update(data).then((value) {
          return true;
        }).catchError((error) {
          throw ('FireStore error: $error');
        });
      }
    } catch (e) {
      print('error ${e.toString()}');
      throw ('error ${e.toString()}');
    }
  }

  Future<bool> deleteData(String dataId) {
    CollectionReference users = FirebaseFirestore.instance.collection(collectionName);
    try {
      return users.doc(dataId).delete().then((value) {
        return true;
      }).catchError((error) {
        throw ('FireStore error: $error');
      });
    } catch (e) {
      print('error ${e.toString()}');
      throw ('error ${e.toString()}');
    }
  }

  Stream<List<Guest>> streamUsers() {
    var result = getDataStream();
    _streamSubscription = result.listen((values) {
      _streamController.add(values);
    });
    return result;
  }

  Stream<List<Guest>> getDataStream() {
    var data = FirebaseFirestore.instance.collection(collectionName).snapshots().map(
          (event) => event.docs.map((e) {
        Map<String, dynamic> document = e.data();
        document['id'] = e.id;
        return Guest.fromJson(document);
      },).toList(),
    );
    return data;
  }

  void dispose() {
    _streamController?.close();
    _streamSubscription?.cancel();
  }


}