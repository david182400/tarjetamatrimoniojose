import 'package:flutter/material.dart';


class CreateListItem extends StatelessWidget {
  final int index;
  final Function onDelete;
  final Widget child;

  const CreateListItem({
    @required this.index,
    @required this.onDelete,
    @required this.child
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            color: Colors.lightBlueAccent),
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: Row(
          children: [
            Container(child: child, width: 250,),
            Expanded(child: Container()),
            IconButton(
              icon: Icon(Icons.delete, color: Colors.red, size: 30,),
              onPressed: onDelete,
            ),
            SizedBox(width: 20,)
          ],
        ),
      ),
    );
  }
}