class ImagesModel {

  int index;
  String image;

  ImagesModel(this.index, this.image);

}

List<ImagesModel> imagesList = [
  ImagesModel(1, 'assets/image1.jpg'),
  ImagesModel(2, 'assets/image2.jpg'),
  ImagesModel(3, 'assets/image3.jpg'),
  ImagesModel(4, 'assets/image4.jpg'),
  ImagesModel(5, 'assets/image5.jpg'),
  ImagesModel(6, 'assets/image6.jpg'),
  ImagesModel(7, 'assets/image7.jpg'),
  ImagesModel(8, 'assets/image8.jpg'),
  ImagesModel(9, 'assets/image9.jpg'),
];