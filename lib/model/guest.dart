class Guest {

  String id;
  String name;
  int guestCount;

  Guest.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    name = jsonMap['GuestName'];
    guestCount = jsonMap['GuestCount'];
  }

}