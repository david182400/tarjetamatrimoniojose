import 'package:flutter/material.dart';
import 'package:tarjeta_matrimonio/pages/admin_page.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _userEditingController = TextEditingController();
  TextEditingController _passEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(right: 60, left: 60, bottom: 100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _userEditingController,
              cursorHeight: 20,
              decoration: InputDecoration(
                hintText: 'User',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                isDense: true
              ),
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(height: 20,),
            TextField(
              obscureText: true,
              controller: _passEditingController,
              cursorHeight: 20,
              decoration: InputDecoration(
                hintText: 'Password',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                isDense: true
              ),
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('LOGIN'),
        onPressed: (){
          if (_userEditingController.text.isNotEmpty && _passEditingController.text.isNotEmpty) {
            if (_userEditingController.text == 'admin' && _passEditingController.text == 'passadmin123') {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminPage(),)).then((value) {
                _userEditingController.text = '';
                _passEditingController.text = '';
              });
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('Datos incorrectos'))
              );
            }
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('Ingrese todos los datos'))
            );
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
