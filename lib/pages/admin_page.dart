import 'package:flutter/material.dart';
import 'package:tarjeta_matrimonio/firestore_source/firestore_api.dart';
import 'package:tarjeta_matrimonio/model/guest.dart';
import 'package:tarjeta_matrimonio/utils/item_list.dart';


class AdminPage extends StatefulWidget {
  const AdminPage({Key key}) : super(key: key);

  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  List<Guest> guestsList = [];
  FirestoreApi _firestoreApi = FirestoreApi('GUESTS');

  @override
  void initState() {
    super.initState();
    _firestoreApi.streamUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gestionar Invitados'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
        child: StreamBuilder(
          stream: _firestoreApi.listGuestsStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              guestsList = snapshot.data;
              return ListView.builder(
                itemCount: guestsList.length,
                itemBuilder: (context, index) {
                  return CreateListItem(
                      index: index,
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: ListTile(
                          title: Text('Nombre: ${guestsList[index].name}', style: TextStyle(fontSize: 24),),
                          subtitle: Text('Cantidad: ${guestsList[index].guestCount.toString()}', style: TextStyle(fontSize: 20),),
                        ),
                      ),
                      onDelete: (){
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text('INFORMACIÓN!', style: TextStyle(fontSize: 24),),
                              content: Text('Desea eliminar el registro?', style: TextStyle(fontSize: 20),),
                              actions: [
                                TextButton(
                                  child: Text('Cancelar', style: TextStyle(fontSize: 18),),
                                  onPressed: (){
                                    Navigator.pop(context);
                                  },
                                ),
                                SizedBox(width: 50.0,),
                                TextButton(
                                  child: Text('Eliminar', style: TextStyle(fontSize: 18)),
                                  onPressed: () {
                                    _firestoreApi.deleteData(guestsList[index].id).then((value) {
                                      Navigator.pop(context);
                                      ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(content: Text('Registro eliminado'))
                                      );
                                    }).catchError((error) {
                                      ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(content: Text('Error: $error'))
                                      );
                                    });
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                  );
                },
              );
            } else {
              return Center(child: CircularProgressIndicator(),);
            }
          }
        ),
      ),
    );
  }
}
