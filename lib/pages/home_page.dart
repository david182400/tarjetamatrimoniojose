import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:tarjeta_matrimonio/firestore_source/firestore_api.dart';
import 'package:tarjeta_matrimonio/model/images_model.dart';
import 'package:tarjeta_matrimonio/utils/utils.dart';



class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Color.fromRGBO(255, 245, 238, 1),
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 450,
                  child: Stack(
                    children: [
                      CarouselSlider(
                        items: generateItems(),
                        options: CarouselOptions(
                          enlargeCenterPage: true,
                          autoPlay: true,
                          height: 450,
                        ),
                        carouselController: _controller,
                      ),
                      Positioned(
                        child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                                padding: EdgeInsets.only(bottom: 20.0),
                                child: Text('JOSE\nY\nKAREN', style: TextStyle(fontSize: 50, color: Colors.white,), textAlign: TextAlign.center,)
                            )
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 60),
                  child: Text(
                    '"Nos complace que seas parte de esta '
                    'nueva etapa de nuestras vidas, esperamos que '
                    'nos puedan acompañar en este día tan especial"',
                    style: TextStyle(fontSize: 30, color: Colors.black,), textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 20,),
                Image.asset('assets/separator1.png', height: 80,),
                TimeCounter(),
                BannerMap(),
                Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    child: Image.asset('assets/separator2.png', height: 60,)
                ),
                RegisterArea(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> generateItems(){
    return imagesList.map((element) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(10)
        ),
        child: ClipRRect(
          child: Opacity(
            opacity: 0.7,
            child: Image.asset(
              element.image,
              fit: BoxFit.cover,
            ),
          ),
          borderRadius: BorderRadius.circular(10),
        ),
      );
    }).toList();
  }

}

class TimeCounter extends StatelessWidget {
  final CountdownTimerController controller;
  const TimeCounter({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var secondsToDate = DateTime.now().difference(DateTime(2021, 8, 6)).inSeconds * -1;
    var endTime = DateTime.now().millisecondsSinceEpoch + 1000 * secondsToDate;
    return Container(
      padding: EdgeInsets.only(top: 10),
      alignment: Alignment.center,
      margin: EdgeInsets.all(10),
      height: 70,
      width: double.infinity,
      decoration: BoxDecoration(),
      child: CountdownTimer(
        endTime: endTime,
        widgetBuilder: (_, CurrentRemainingTime time) {
          if (time == null) {
            return Text('Game over');
          }
          var days = time.days.toString().length < 2 ? '0' + time.days.toString() : time.days.toString();
          var hours = time.hours.toString().length < 2 ? '0' + time.hours.toString() : time.hours.toString();
          var minutes = time.min.toString().length < 2 ? '0' + time.min.toString() : time.min.toString();
          var seconds = time.sec.toString().length < 2 ? '0' + time.sec.toString() : time.sec.toString();
          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Text('$days', style: TextStyle(fontSize: 24),),
                      Text('DÍAS', style: TextStyle(fontSize: 15),),
                    ],
                  ),
                  Column(
                    children: [
                      Text('$hours', style: TextStyle(fontSize: 24),),
                      Text('HORAS', style: TextStyle(fontSize: 15),),
                    ],
                  ),
                  Column(
                    children: [
                      Text('$minutes', style: TextStyle(fontSize: 24),),
                      Text('MIN', style: TextStyle(fontSize: 15),),
                    ],
                  ),
                  Column(
                    children: [
                      Text('$seconds', style: TextStyle(fontSize: 24),),
                      Text('SEG', style: TextStyle(fontSize: 15),),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}


class BannerMap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 110.0,
          width: MediaQuery.of(context).size.width - 30,
          decoration: BoxDecoration(
            color: Colors.black,
              borderRadius: BorderRadius.circular(20.0),
              image: DecorationImage(
                  image: AssetImage('assets/place_image.jpg'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.6), BlendMode.dstATop)
              )
          ),
        ),
        Positioned(
          top: 10.0,
          left: 20.0,
          child: Text('Lugar de la recepción', style: TextStyle(color: Colors.white, fontSize: 30.0, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 50.0,
          left: 20.0,
          child: Text('Clasicos Forever', style: TextStyle(color: Colors.white, fontSize: 22.0,)),
        ),
        Positioned(
          top: 80.0,
          left: 20.0,
          child: Text('6 p.m', style: TextStyle(color: Colors.white, fontSize: 16.0,)),
        ),
        Positioned(
          top: 20.0,
          right: 20.0,
          child: IconButton(
            icon: Icon(Icons.location_on_outlined, color: Colors.white, size: 50.0,),
            onPressed: (){
              Utils.launchMap('Clasicos Forever');
            },
          ),
        )
      ],
    );
  }

}


class RegisterArea extends StatefulWidget {
  const RegisterArea({Key key}) : super(key: key);

  @override
  _RegisterAreaState createState() => _RegisterAreaState();
}

class _RegisterAreaState extends State<RegisterArea> {
  String dropdownValue = '1';
  FirestoreApi _firestoreApi = FirestoreApi('GUESTS');
  TextEditingController _textEditingController = TextEditingController();

  @override
  void dispose() {
    _textEditingController.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/fondo1.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.dstATop)
        ),
        color: Colors.black,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        children: [
          SizedBox(height: 20,),
          Text('CONFIRMAR AQUÍ!', style: TextStyle(fontSize: 35, color: Colors.white),),
          SizedBox(height: 30.0,),
          Text('Por favor confirma antes del 28 de Julio.', style: TextStyle(fontSize: 20, color: Colors.white), textAlign: TextAlign.center,),
          SizedBox(height: 50.0,),
          TextField(
            controller: _textEditingController,
            cursorColor: Colors.white60,
            cursorHeight: 30,
            decoration: InputDecoration(
              hintText: 'Nombre',
              hintStyle: TextStyle(fontSize: 22, color: Colors.white54),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white54)
              ),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)
              ),
            ),
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
          SizedBox(height: 20,),
          Row(
            children: [
              Text('Invitados ', style: TextStyle(fontSize: 22, color: Colors.white54),),
              SizedBox(width: 30,),
              Flexible(
                child: DropdownButton(
                  isExpanded: true,
                  value: dropdownValue,
                  elevation: 16,
                  dropdownColor: Colors.black54,
                  icon: Icon(Icons.arrow_drop_down, color: Colors.white,),
                  underline: Container(
                    height: 1,
                    color: Colors.white54,
                  ),
                  items: [
                    DropdownMenuItem(child: Text('1', style: TextStyle(fontSize: 22, color: Colors.white),), value: '1',),
                    DropdownMenuItem(child: Text('2', style: TextStyle(fontSize: 22, color: Colors.white),), value: '2',),
                  ],
                  onChanged: (value) {
                    setState(() {
                      dropdownValue = value;
                    });
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Text('Por favor ingresa toda la información solicitada', style: TextStyle(fontSize: 14, color: Colors.white), textAlign: TextAlign.center,),
          SizedBox(height: 50,),
          TextButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              decoration: BoxDecoration(
                color: Colors.teal,
                borderRadius: BorderRadius.circular(20)
              ),
              child: Text('Registrar', style: TextStyle(color: Colors.white, fontSize: 18),),
            ),
            onPressed: (){
              if (_textEditingController.text.isNotEmpty) {
                Map<String, dynamic> data = {
                  'GuestName' : _textEditingController.text,
                  'GuestCount' : int.parse(dropdownValue)
                };
                _firestoreApi.addEditData(data, true).then((value) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Usuario registrado con éxito'))
                  );
                  _textEditingController.text = '';
                });
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Debe llenar todos los campos'))
                );
              }
            },
          )
        ],
      ),
    );
  }
}

